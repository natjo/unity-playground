using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexMapCamera : MonoBehaviour {
    public float stickMinZoom, stickMaxZoom;
    public float swivelMinZoom, swivelMaxZoom;
    public float moveSpeedMinZoom, moveSpeedMaxZoom;
    public float rotationSpeed;

    Transform swivel, stick;
    float zoom = 0.8f;
    float rotationAngle;

    Vector3 PreviousClickPosition;
    Vector3 PreviousWorldPoint;
    void Awake() {
        swivel = transform.GetChild(0);
        stick = swivel.GetChild(0);

        // Init zoom
        AdjustZoom(0f);
    }
    void Update() {
        // Update Zoom
        float zoomDelta = Input.GetAxis("Mouse ScrollWheel");
        if (zoomDelta != 0) {
            AdjustZoom(zoomDelta);
        }

        float rotationDelta = Input.GetAxis("Rotation");
        if (rotationDelta != 0) {
            AdjustRotation(rotationDelta);
        }

        bool isDown = Input.GetMouseButton(1);
        bool wentDown = Input.GetMouseButtonDown(1);

        if (wentDown || isDown) {
            DragMap(isDown, wentDown);
        }

        float xDelta = Input.GetAxis("Horizontal");
        float zDelta = Input.GetAxis("Vertical");
        if (xDelta != 0f || zDelta != 0f) {
            AdjustPosition(xDelta, zDelta);
        }
    }

    void AdjustZoom(float delta) {
        zoom = Mathf.Clamp01(zoom + delta);
        float distance = Mathf.Lerp(stickMinZoom, stickMaxZoom, zoom);
        Vector3 stickPosition = stick.localPosition;
        stick.localPosition = new Vector3(stickPosition.x, stickPosition.y, distance);

        float angle = Mathf.Lerp(swivelMinZoom, swivelMaxZoom, zoom);
        swivel.localRotation = Quaternion.Euler(angle, 0f, 0f);
    }

    void AdjustRotation(float delta) {
        rotationAngle -= delta * rotationSpeed * Time.deltaTime;
        if (rotationAngle < 0f) {
            rotationAngle += 360f;
        }
        if (rotationAngle > 360f) {
            rotationAngle -= 360f;
        }
        transform.localRotation = Quaternion.Euler(0f, rotationAngle, 0f);
    }

    void DragMap(bool isDown, bool wentDown) {
        Vector3 clickPosition = Input.mousePosition;
        // Don't drag the mouse if the user only clicked, and did not hold down yet
        if (!wentDown) {
            float distance = 0f;
            Vector3 worldPoint = GetCastedToGroundPoint(clickPosition, out distance);

            if (distance > 0) {
                if (isDown) {
                    // recalculate the PreviousClickPosition using the current camera position
                    PreviousWorldPoint = GetCastedToGroundPoint(PreviousClickPosition, out distance);

                    Vector3 worldDelta = PreviousWorldPoint - worldPoint;
                    AdjustPosition(worldDelta);
                }
                PreviousWorldPoint = worldPoint;
            }
        }
        PreviousClickPosition = clickPosition;
    }

    Vector3 GetCastedToGroundPoint(Vector3 point, out float distance) {
        // this casts the provided point into the ground plain where it is in the world. This just casts it to a Plane at y == 0
        var ray = Camera.main.ScreenPointToRay(point);
        var plane = new Plane(Vector3.up, Vector3.zero);
        plane.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }

    void AdjustPosition(float xDelta, float zDelta) {
        Vector3 direction =
            transform.localRotation *
            new Vector3(xDelta, 0f, zDelta).normalized;
        float damping = Mathf.Max(Mathf.Abs(xDelta), Mathf.Abs(zDelta));
        float distance = Mathf.Lerp(moveSpeedMinZoom, moveSpeedMaxZoom, zoom) * damping * Time.deltaTime;

        AdjustPosition(direction * distance);
    }

    void AdjustPosition(Vector3 Delta) {
        Vector3 position = transform.localPosition;
        position += Delta;
        transform.localPosition = position;
    }
}
