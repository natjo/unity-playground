using UnityEngine;

public struct EdgeVertices {
    // Contains information about the outer line of a cell sixth, 
    // which are split in quarters for finer details
    // The vertices are going clockwise

    public Vector3 v1, v2, v3, v4, v5;
    public EdgeVertices(Vector3 corner1, Vector3 corner2) {
        v1 = corner1;
        v2 = Vector3.Lerp(corner1, corner2, 1f / 4f);
        v3 = Vector3.Lerp(corner1, corner2, 2f / 4f);
        v4 = Vector3.Lerp(corner1, corner2, 3f / 4f);
        v5 = corner2;
    }

    public EdgeVertices(Vector3 corner1, Vector3 corner2, float outerStep) {
        v1 = corner1;
        v2 = Vector3.Lerp(corner1, corner2, outerStep);
        v3 = Vector3.Lerp(corner1, corner2, 0.5f);
        v4 = Vector3.Lerp(corner1, corner2, 1f - outerStep);
        v5 = corner2;
    }

    public static EdgeVertices Lerp(EdgeVertices a, EdgeVertices b, float interpolant){
        EdgeVertices result;
        result.v1 = Vector3.Lerp(a.v1, b.v1, interpolant);
        result.v2 = Vector3.Lerp(a.v2, b.v2, interpolant);
        result.v3 = Vector3.Lerp(a.v3, b.v3, interpolant);
        result.v4 = Vector3.Lerp(a.v4, b.v4, interpolant);
        result.v5 = Vector3.Lerp(a.v5, b.v5, interpolant);
        return result;
    }
}