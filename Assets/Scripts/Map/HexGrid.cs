using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HexGrid : MonoBehaviour {
    public HexCell cellPrefab;
    public HexGridChunk chunkPrefab;
    public Text cellLabelPrefab;
    public Texture2D noiseSource;
    public int chunkCountX = 4, chunkCountZ = 3;

    int cellCountX, cellCountZ;
    HexCell[] cells;
    HexGridChunk[] chunks;

    Color defaultColor = Color.blue;

    void Awake() {
        cellCountX = chunkCountX * HexMetrics.chunkSizeX;
        cellCountZ = chunkCountZ * HexMetrics.chunkSizeZ;

        CreateChunks();
        CreateCells();

        HexMetrics.noiseSource = noiseSource;

        for (int z = 2; z < cellCountZ - 2; ++z) {
            for (int x = 2; x < cellCountX - 2; ++x) {
                cells[x + z * cellCountX].Color = Color.yellow;
                cells[x + z * cellCountX].Elevation = 1;
            }
        }

        for (int z = 4; z < cellCountZ - 4; ++z) {
            for (int x = 4; x < cellCountX - 8; ++x) {
                cells[x + z * cellCountX].Color = Color.green;
                cells[x + z * cellCountX].Elevation = 4;
            }
        }

        for (int z = 7; z < 9; ++z) {
            for (int x = 6; x < cellCountX - 10; ++x) {
                cells[x + z * cellCountX].Color = Color.white;
                cells[x + z * cellCountX].Elevation = 5;
            }
        }

        cells[4 + 4 * cellCountX].Elevation = 5;
        cells[3 + 5 * cellCountX].Elevation = 3;


        cells[5 + 7 * cellCountX].AddRiver(HexDirection.NW, true);


        cells[4 + 10 * cellCountX].AddRiver(HexDirection.NE, false);
        cells[4 + 10 * cellCountX].AddRiver(HexDirection.E, false);
        cells[4 + 10 * cellCountX].AddRiver(HexDirection.SE, true);
        cells[3 + 9 * cellCountX].AddRiver(HexDirection.E, true);

        cells[5 + 9 * cellCountX].AddRiver(HexDirection.SE, false);
        cells[5 + 9 * cellCountX].AddRiver(HexDirection.SW, false);

        cells[8 + 8 * cellCountX].AddRiver(HexDirection.NW, true);
        cells[7 + 8 * cellCountX].AddRiver(HexDirection.NW, false);
        // cells[7 + 8 * cellCountX].AddRiver(HexDirection.NE, false);
        cells[7 + 8 * cellCountX].AddRiver(HexDirection.E, false);

        cells[6 + 7 * cellCountX].AddRiver(HexDirection.E, true);
        cells[6 + 7 * cellCountX].AddRiver(HexDirection.SE, true);
        cells[6 + 6 * cellCountX].AddRiver(HexDirection.E, true);
        cells[6 + 6 * cellCountX].AddRiver(HexDirection.SE, true);
        cells[5 + 5 * cellCountX].AddRiver(HexDirection.E, true);
        cells[5 + 5 * cellCountX].AddRiver(HexDirection.SE, true);
        cells[5 + 5 * cellCountX].AddRiver(HexDirection.SW, false);
        cells[5 + 4 * cellCountX].AddRiver(HexDirection.E, true);
        cells[5 + 4 * cellCountX].AddRiver(HexDirection.SE, true);
        // cells[6 + 4 * cellCountX].AddRiver(HexDirection.SW, false);

        cells[4 + 5 * cellCountX].AddRiver(HexDirection.SE, true);
        cells[4 + 5 * cellCountX].AddRiver(HexDirection.SW, true);
        cells[3 + 5 * cellCountX].AddRiver(HexDirection.E, true);
        cells[3 + 5 * cellCountX].AddRiver(HexDirection.SE, true);

        cells[5 + 3 * cellCountX].AddRoad(HexDirection.W);
        cells[4 + 3 * cellCountX].AddRoad(HexDirection.SW);
        cells[4 + 3 * cellCountX].AddRoad(HexDirection.W);
        cells[3 + 3 * cellCountX].AddRoad(HexDirection.SW);
        cells[3 + 2 * cellCountX].AddRoad(HexDirection.E);
        cells[4 + 2 * cellCountX].AddRoad(HexDirection.SE);



        cells[8 + 7 * cellCountX].Elevation = 6;
        cells[8 + 7 * cellCountX].AddRiver(HexDirection.E, true);
        cells[7 + 5 * cellCountX].AddRiver(HexDirection.NW, true);
        cells[8 + 6 * cellCountX].Elevation = 3;
        cells[8 + 6 * cellCountX].WaterLevel = 4;
        cells[9 + 6 * cellCountX].Elevation = 3;
        cells[9 + 6 * cellCountX].WaterLevel = 4;
        cells[10 + 6 * cellCountX].Elevation = 3;
        cells[10 + 6 * cellCountX].WaterLevel = 4;
        cells[8 + 5 * cellCountX].Elevation = 3;
        cells[8 + 5 * cellCountX].WaterLevel = 4;
        cells[9 + 5 * cellCountX].Elevation = 3;
        cells[9 + 5 * cellCountX].WaterLevel = 4;
        cells[10 + 5 * cellCountX].Elevation = 3;
        cells[10 + 5 * cellCountX].WaterLevel = 4;

        cells[14 + 5 * cellCountX].Elevation = 0;
        cells[14 + 5 * cellCountX].WaterLevel = 1;
        cells[15 + 5 * cellCountX].Elevation = 0;
        cells[15 + 5 * cellCountX].WaterLevel = 1;
        cells[16 + 5 * cellCountX].Elevation = 0;
        cells[16 + 5 * cellCountX].WaterLevel = 1;
        cells[15 + 6 * cellCountX].Elevation = 0;
        cells[15 + 6 * cellCountX].WaterLevel = 1;
        cells[16 + 6 * cellCountX].Elevation = 0;
        cells[16 + 6 * cellCountX].WaterLevel = 1;

    }

    void OnEnable() {
        HexMetrics.noiseSource = noiseSource;
    }

    void CreateCells() {
        cells = new HexCell[cellCountZ * cellCountX];
        for (int z = 0, i = 0; z < cellCountZ; z++) {
            for (int x = 0; x < cellCountX; x++) {
                CreateCell(x, z, i++);
            }
        }
    }

    void CreateChunks() {
        chunks = new HexGridChunk[chunkCountX * chunkCountZ];
        for (int z = 0, i = 0; z < chunkCountZ; z++) {
            for (int x = 0; x < chunkCountX; x++) {
                HexGridChunk chunk = chunks[i++] = Instantiate(chunkPrefab);
                chunk.transform.SetParent(transform);
            }
        }
    }

    void CreateCell(int x, int z, int i) {
        Vector3 position = GetAbsoluteCellPosition(x, z, i);

        HexCell cell = cells[i] = Instantiate<HexCell>(cellPrefab);
        cell.transform.localPosition = position;
        cell.coordinates = HexCoordinates.FromOffsetCoordinates(x, z);
        cell.Color = defaultColor;

        // Set neighbors
        if (x > 0) {
            cell.SetNeighbor(HexDirection.W, cells[i - 1]);
        }
        if (z > 0) {
            if ((z & 1) == 0) {
                cell.SetNeighbor(HexDirection.SE, cells[i - cellCountX]);
                if (x > 0) {
                    cell.SetNeighbor(HexDirection.SW, cells[i - cellCountX - 1]);
                }
            } else {
                cell.SetNeighbor(HexDirection.SW, cells[i - cellCountX]);
                if (x < cellCountX - 1) {
                    cell.SetNeighbor(HexDirection.SE, cells[i - cellCountX + 1]);
                }
            }
        }

        Text label = CreateLabelForCell(cell);
        cell.uiRect = label.rectTransform;
        cell.Elevation = 0;

        AddCellToChunk(x, z, cell);
    }

    void AddCellToChunk(int x, int z, HexCell cell) {
        int chunkX = x / HexMetrics.chunkSizeX;
        int chunkZ = z / HexMetrics.chunkSizeZ;
        HexGridChunk chunk = chunks[chunkX + chunkZ * chunkCountX];

        int localX = x - chunkX * HexMetrics.chunkSizeX;
        int localZ = z - chunkZ * HexMetrics.chunkSizeZ;
        chunk.AddCell(localX + localZ * HexMetrics.chunkSizeX, cell);
    }

    Vector3 GetAbsoluteCellPosition(int x, int z, int i) {
        // Computes the absolute position of a cell in the environment
        Vector3 position;
        float x_tile_width = (HexMetrics.innerRadius * 2f);
        float z_tile_width = (HexMetrics.outerRadius * 1.5f);
        int x_offset = z / 2;
        position.x = (x + z * 0.5f - x_offset) * x_tile_width;
        position.y = 0f;
        position.z = z * z_tile_width;
        return position;
    }

    Text CreateLabelForCell(HexCell cell) {
        Vector3 position = cell.transform.localPosition;
        Text label = Instantiate<Text>(cellLabelPrefab);
        label.rectTransform.anchoredPosition = new Vector2(position.x, position.z);
        label.text = cell.coordinates.ToStringOnSeparateLines();
        return label;
    }

    public HexCell GetCell(Vector3 position) {
        position = transform.InverseTransformPoint(position);
        HexCoordinates coordinates = HexCoordinates.FromPosition(position);
        int index = GetCellIndexFromCoordinates(coordinates);
        return cells[index];
    }

    public HexCell GetCell(HexCoordinates coordinates) {
        int z = coordinates.Z;
        int x = coordinates.X + z / 2;
        if (z < 0 || z >= cellCountZ) {
            return null;
        }
        if (x < 0 || x >= cellCountX) {
            return null;
        }
        return cells[x + z * cellCountX];
    }

    int GetCellIndexFromCoordinates(HexCoordinates coordinates) {
        return coordinates.X + coordinates.Z * cellCountX + coordinates.Z / 2;
    }

    public void ShowUI(bool visible) {
        for (int i = 0; i < chunks.Length; ++i) {
            chunks[i].ShowUI(visible);
        }
    }
}
