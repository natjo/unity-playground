using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexGridChunk : MonoBehaviour {

    HexCell[] cells;
    public HexMesh terrain, rivers, roads, water, waterShore, estuaries;
    Canvas gridCanvas;

    void Awake() {
        gridCanvas = GetComponentInChildren<Canvas>();
        cells = new HexCell[HexMetrics.chunkSizeX * HexMetrics.chunkSizeZ];
        ShowUI(false);
    }

    public void Refresh() {
        enabled = true;
    }

    void LateUpdate() {
        Triangulate();
        enabled = false;
    }

    public void AddCell(int index, HexCell cell) {
        cells[index] = cell;
        cell.chunk = this;
        cell.transform.SetParent(transform, false);
        cell.uiRect.SetParent(gridCanvas.transform, false);
    }

    public void ShowUI(bool visible) {
        gridCanvas.gameObject.SetActive(visible);
    }

    public void Triangulate() {
        terrain.Clear();
        rivers.Clear();
        roads.Clear();
        water.Clear();
        waterShore.Clear();
        estuaries.Clear();
        for (int i = 0; i < cells.Length; i++) {
            Triangulate(cells[i]);
        }
        terrain.Apply();
        rivers.Apply();
        roads.Apply();
        water.Apply();
        waterShore.Apply();
        estuaries.Apply();
    }

    void Triangulate(HexCell cell) {
        for (HexDirection d = HexDirection.NE; d <= HexDirection.NW; d++) {
            Triangulate(d, cell);
        }
    }

    void Triangulate(HexDirection direction, HexCell cell) {
        // direction specifies the direction the triangle is facing away from the center
        Vector3 center = cell.Position;

        // Each sixth of the hexagon is split in four parts for finer grained resolution
        EdgeVertices e = new EdgeVertices(
            center + HexMetrics.GetFirstSolidCorner(direction),
            center + HexMetrics.GetSecondSolidCorner(direction)
            );

        TriangulateCenter(direction, cell, center, e);

        if (direction <= HexDirection.SE) {
            TriangulateConnection(direction, cell, e);
        }

        if (cell.IsUnderwater) {
            TriangulateWater(direction, cell, center);
        }
    }

    void TriangulateCenter(HexDirection direction, HexCell cell, Vector3 center, EdgeVertices e) {
        TriangulateEdgeFan(center, e, cell.Color);

        if (cell.HasRoads) {
            Vector2 interpolators = GetRoadInterpolators(direction, cell);
            TriangulateRoad(
                center,
                Vector3.Lerp(center, e.v1, interpolators.x),
                Vector3.Lerp(center, e.v5, interpolators.y),
                e, cell.HasRoadThroughEdge(direction));
        }
    }

    void TriangulateConnection(HexDirection direction, HexCell cell, EdgeVertices e1) {
        HexCell neighbor = cell.GetNeighbor(direction);
        // Draws everything around the inner cell part
        // Ignore connections out of the field (i.e. without a neigbor)
        if (neighbor == null) {
            return;
        }

        Vector3 bridge = HexMetrics.GetBridge(direction);
        bridge.y = neighbor.Position.y - cell.Position.y;
        EdgeVertices e2 = new EdgeVertices(
            e1.v1 + bridge,
            e1.v5 + bridge
        );

        // Triangulate the squared part of the edge
        if (cell.HasRiver(direction)) {
            // Find the lower one of the two cells between the river
            float riverSurfaceY = cell.RiverSurfaceYShared(direction);
            float riverStreamBedY = cell.RiverStreamBedYShared(direction);
            TriangulateEdgeStripWithRiver(e1, cell.Color, e2, neighbor.Color, cell.GetRiverFlowDirection(direction), riverSurfaceY, riverStreamBedY);
        } else {
            TriangulateEdgeStrip(e1, cell.Color, e2, neighbor.Color, cell.HasRoadThroughEdge(direction));
        }

        // Triangulate the triangle part of an edge (clockwise after the squared part)
        HexCell nextNeighbor = cell.GetNeighbor(direction.Next());
        // Only for the first two sides, add a triangle to fill the holes between connection
        if (direction <= HexDirection.E && nextNeighbor != null) {
            Vector3 v5 = e1.v5 + HexMetrics.GetBridge(direction.Next());
            v5.y = nextNeighbor.Position.y;
            TriangulateCorner(e1.v5, cell, e2.v5, neighbor, v5, nextNeighbor, cell.GetRiverFlowDirection(direction), direction);
        }
    }

    void TriangulateCorner(
        Vector3 begin, HexCell beginCell,
        Vector3 left, HexCell leftCell,
        Vector3 right, HexCell rightCell,
        bool flowsClockwise, HexDirection direction
    ) {
        // Draws the corners on the outside of a cell, connecting the EdgeStrips
        HexConnectionWithRiverInformation conInfo =
            new HexConnectionWithRiverInformation(begin, beginCell, left, leftCell, right, rightCell, direction);

        if (conInfo.withRiver) {
            // River requires more information points, which we can calculate here

            if (conInfo.riverEnds) {
                TriangulateCornerWithRiverEnd(begin, beginCell, left, leftCell, right, rightCell, flowsClockwise, conInfo);
            } else {
                TriangulateCornerWithRiver(begin, beginCell, left, leftCell, right, rightCell, flowsClockwise, conInfo);
            }

            TriangulateOptionalWaterfall(begin, beginCell, left, leftCell, right, rightCell, conInfo, direction);

            if (!beginCell.IsUnderwater && !leftCell.IsUnderwater && !rightCell.IsUnderwater) {
                TriangulateRiverTriangle(begin, left, right, conInfo.RiverSurfaceY, flowsClockwise);
            }
        } else {
            terrain.AddTriangle(begin, left, right);
            terrain.AddTriangleColor(beginCell.Color, leftCell.Color, rightCell.Color);
        }
    }


    void TriangulateWater(HexDirection direction, HexCell cell, Vector3 center) {
        center.y = cell.WaterSurfaceY;
        HexCell neighbor = cell.GetNeighbor(direction);
        if (neighbor == null || !neighbor.IsUnderwater) {
            TriangulateWaterShore(direction, cell, neighbor, center);
        } else {
            TriangulateOpenWater(direction, cell, neighbor, center);
        }
    }

    void TriangulateOpenWater(HexDirection direction, HexCell cell, HexCell neighbor, Vector3 center) {
        Vector3 c1 = center + HexMetrics.GetFirstWaterCorner(direction);
        Vector3 c2 = center + HexMetrics.GetSecondWaterCorner(direction);

        // Add main cell
        water.AddTriangle(center, c1, c2);

        // Add edge
        if (direction <= HexDirection.SE) {
            Vector3 bridge = HexMetrics.GetWaterBridge(direction);
            Vector3 e1 = c1 + bridge;
            Vector3 e2 = c2 + bridge;
            water.AddQuad(c1, c2, e1, e2);

            // Add corner
            if (direction <= HexDirection.E) {
                HexCell nextNeighbor = cell.GetNeighbor(direction.Next());
                if (nextNeighbor == null || !nextNeighbor.IsUnderwater) {
                    return;
                }
                water.AddTriangle(
                    c2, e2, c2 + HexMetrics.GetWaterBridge(direction.Next())
                );
            }
        }
    }

    void TriangulateWaterShore(HexDirection direction, HexCell cell, HexCell neighbor, Vector3 center) {
        HexCell nextNeighbor = cell.GetNeighbor(direction.Next());
        EdgeVertices e1 = new EdgeVertices(
            center + HexMetrics.GetFirstWaterCorner(direction),
            center + HexMetrics.GetSecondWaterCorner(direction)
        );

        // First and second seen corners clockwise
        bool hasEstuaryFirst = neighbor.HasRiver(direction.Opposite().Previous());
        bool hasEstuarySecond = neighbor.HasRiver(direction.Opposite().Next());
        water.AddTriangle(center, e1.v1, e1.v2);
        water.AddTriangle(center, e1.v2, e1.v3);
        water.AddTriangle(center, e1.v3, e1.v4);
        water.AddTriangle(center, e1.v4, e1.v5);

        Vector3 center2 = neighbor.Position;
        center2.y = center.y;
        EdgeVertices e2 = new EdgeVertices(
            center2 + HexMetrics.GetSecondSolidCorner(direction.Opposite()),
            center2 + HexMetrics.GetFirstSolidCorner(direction.Opposite())
        );
        if (hasEstuaryFirst) {
            // Left side of the estuary
            estuaries.AddQuad(e1.v4, e1.v5, e2.v4, e2.v5);
            estuaries.AddQuadUV(0f, 0f, 0f, 1f);
        } else {
            waterShore.AddQuad(e1.v4, e1.v5, e2.v4, e2.v5);
            waterShore.AddQuadUV(0f, 0f, 0f, 1f);
        }
        if (hasEstuarySecond) {
            // Right side of the estuary
            estuaries.AddQuad(e1.v1, e1.v2, e2.v1, e2.v2);
            estuaries.AddQuadUV(0f, 0f, 0f, 1f);
        } else {
            waterShore.AddQuad(e1.v1, e1.v2, e2.v1, e2.v2);
            waterShore.AddQuadUV(0f, 0f, 0f, 1f);
        }
        waterShore.AddQuad(e1.v2, e1.v3, e2.v2, e2.v3);
        waterShore.AddQuad(e1.v3, e1.v4, e2.v3, e2.v4);
        waterShore.AddQuadUV(0f, 0f, 0f, 1f);
        waterShore.AddQuadUV(0f, 0f, 0f, 1f);

        if (nextNeighbor != null) {
            Vector3 v3 = nextNeighbor.Position + (nextNeighbor.IsUnderwater ?
                HexMetrics.GetFirstWaterCorner(direction.Previous()) :
                HexMetrics.GetFirstSolidCorner(direction.Previous()));
            v3.y = center.y;
            if (hasEstuaryFirst) {
                estuaries.AddTriangle(e1.v5, e2.v5, v3);
                estuaries.AddTriangleUV(
                    new Vector2(0f, 0f),
                    new Vector2(0f, 1f),
                    new Vector2(0f, nextNeighbor.IsUnderwater ? 0f : 1f)
                );
            } else {
                waterShore.AddTriangle(e1.v5, e2.v5, v3);
                waterShore.AddTriangleUV(
                    new Vector2(0f, 0f),
                    new Vector2(0f, 1f),
                    new Vector2(0f, nextNeighbor.IsUnderwater ? 0f : 1f)
                );
            }
        }
    }

    void TriangulateRoad(Vector3 center, Vector3 mL, Vector3 mR, EdgeVertices e, bool hasRoadThroughCellEdge) {
        if (hasRoadThroughCellEdge) {
            Vector3 mC = Vector3.Lerp(mL, mR, 0.5f);
            TriangulateRoadSegment(mL, mC, mR, e.v2, e.v3, e.v4);
            roads.AddTriangle(center, mL, mC);
            roads.AddTriangle(center, mC, mR);
            roads.AddTriangleUV(new Vector2(1f, 0f), new Vector2(0f, 0f), new Vector2(1f, 0f));
            roads.AddTriangleUV(new Vector2(1f, 0f), new Vector2(1f, 0f), new Vector2(0f, 0f));
        } else {
            TriangulateRoadEdge(center, mL, mR);
        }
    }

    void TriangulateRoadEdge(Vector3 center, Vector3 mL, Vector3 mR) {
        // Adds a Road in one direction of the center of a cell
        roads.AddTriangle(center, mL, mR);
        roads.AddTriangleUV(new Vector2(1f, 0f), new Vector2(0f, 0f), new Vector2(0f, 0f));
    }

    void TriangulateRoadSegment(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4, Vector3 v5, Vector3 v6) {
        roads.AddQuad(v1, v2, v4, v5);
        roads.AddQuad(v2, v3, v5, v6);
        roads.AddQuadUV(0f, 1f, 0f, 0f);
        roads.AddQuadUV(1f, 0f, 0f, 0f);
    }

    Vector2 GetRoadInterpolators(HexDirection direction, HexCell cell) {
        // Depending if a road leads out of this direction, the inner road part should be drawn larger
        Vector2 interpolators;
        if (cell.HasRoadThroughEdge(direction)) {
            interpolators.x = interpolators.y = 0.5f;
        } else {
            interpolators.x = cell.HasRoadThroughEdge(direction.Previous()) ? 0.5f : 0.25f;
            interpolators.y = cell.HasRoadThroughEdge(direction.Next()) ? 0.5f : 0.25f;
        }
        return interpolators;
    }

    void TriangulateEdgeStripWithRiver(
        EdgeVertices e1, Color c1,
        EdgeVertices e3, Color c3,
        bool flowsClockwise, float riverSurfaceY, float riverStreamBedY
        ) {
        // Get another edge vertices for the river bed
        EdgeVertices e2 = EdgeVertices.Lerp(e1, e3, 0.5f);
        e2.v1.y = e2.v2.y = e2.v3.y = e2.v4.y = e2.v5.y = riverStreamBedY;
        Color c2 = Color.Lerp(c1, c3, 0.5f);

        // First half
        terrain.AddQuad(e1.v1, e1.v2, e2.v1, e2.v2);
        terrain.AddQuadColor(c1, c2);
        terrain.AddQuad(e1.v2, e1.v3, e2.v2, e2.v3);
        terrain.AddQuadColor(c1, c2);
        terrain.AddQuad(e1.v3, e1.v4, e2.v3, e2.v4);
        terrain.AddQuadColor(c1, c2);
        terrain.AddQuad(e1.v4, e1.v5, e2.v4, e2.v5);
        terrain.AddQuadColor(c1, c2);

        //Second half
        terrain.AddQuad(e2.v1, e2.v2, e3.v1, e3.v2);
        terrain.AddQuadColor(c2, c3);
        terrain.AddQuad(e2.v2, e2.v3, e3.v2, e3.v3);
        terrain.AddQuadColor(c2, c3);
        terrain.AddQuad(e2.v3, e2.v4, e3.v3, e3.v4);
        terrain.AddQuadColor(c2, c3);
        terrain.AddQuad(e2.v4, e2.v5, e3.v4, e3.v5);
        terrain.AddQuadColor(c2, c3);

        TriangulateRiverQuad(e1.v1, e1.v5, e3.v1, e3.v5, riverSurfaceY, flowsClockwise);
    }

    void TriangulateCornerWithRiver(
        Vector3 begin, HexCell beginCell,
        Vector3 left, HexCell leftCell,
        Vector3 right, HexCell rightCell,
        bool flowsClockwise,
        HexConnectionWithRiverInformation conInfo
    ) {
        // Split into four triangles for a proper river bed
        // First find correct corners. Do not perturb if there is no river
        Vector3 beginLeft = conInfo.beginLeftOriginPerturbed;
        Vector3 beginRight = conInfo.beginRightOriginPerturbed;
        Vector3 leftRight = conInfo.leftRightOriginPerturbed;
        begin = HexMetrics.Perturb(begin);
        left = HexMetrics.Perturb(left);
        right = HexMetrics.Perturb(right);

        // Don't perturb those points, if they are not in the river bed
        if (conInfo.beginLeftSideHasRiver) {
            beginLeft = HexMetrics.Perturb(conInfo.beginLeft);
        }
        if (conInfo.leftRightSideHasRiver) {
            leftRight = HexMetrics.Perturb(conInfo.leftRight);
        }
        if (conInfo.beginRightSideHasRiver) {
            beginRight = HexMetrics.Perturb(conInfo.beginRight);
        }

        // If a side has no river, we cannot perturb it as it is on a straight line between two corners
        TriangulateCornerWithRiverUnperturbed(begin, beginLeft, left, leftRight, right, beginRight,
            beginCell.Color, conInfo.beginLeftColor, leftCell.Color, conInfo.leftRightColor, rightCell.Color, conInfo.beginRightColor);
    }

    void TriangulateCornerWithRiverUnperturbed(
          Vector3 begin, Vector3 beginLeft, Vector3 left,
            Vector3 leftRight, Vector3 right, Vector3 beginRight,
            Color beginColor, Color beginLeftColor, Color leftColor,
            Color leftRightColor, Color rightColor, Color beginRightColor
            ) {
        terrain.AddTriangleUnperturbed(begin, beginLeft, beginRight);
        terrain.AddTriangleColor(beginColor, beginLeftColor, beginRightColor);
        terrain.AddTriangleUnperturbed(beginLeft, left, leftRight);
        terrain.AddTriangleColor(beginLeftColor, leftColor, leftRightColor);
        terrain.AddTriangleUnperturbed(leftRight, right, beginRight);
        terrain.AddTriangleColor(leftRightColor, rightColor, beginRightColor);
        terrain.AddTriangleUnperturbed(leftRight, beginRight, beginLeft);
        terrain.AddTriangleColor(leftRightColor, beginRightColor, beginLeftColor);

    }
    void TriangulateCornerWithRiverEnd(
        Vector3 begin, HexCell beginCell,
        Vector3 left, HexCell leftCell,
        Vector3 right, HexCell rightCell,
        bool flowsClockwise,
        HexConnectionWithRiverInformation conInfo
    ) {
        if (conInfo.beginLeftSideHasRiver) {
            // River ends towards right corner
            terrain.AddTriangle(conInfo.beginLeft, left, right);
            terrain.AddTriangleColor(conInfo.beginLeftColor, leftCell.Color, rightCell.Color);
            terrain.AddTriangle(conInfo.beginLeft, right, begin);
            terrain.AddTriangleColor(conInfo.beginLeftColor, rightCell.Color, beginCell.Color);

        } else if (conInfo.beginRightSideHasRiver) {
            // River ends towards left corner
            terrain.AddTriangle(begin, left, conInfo.beginRight);
            terrain.AddTriangleColor(beginCell.Color, leftCell.Color, conInfo.beginRightColor);
            terrain.AddTriangle(conInfo.beginRight, left, right);
            terrain.AddTriangleColor(conInfo.beginRightColor, leftCell.Color, rightCell.Color);
        } else if (conInfo.leftRightSideHasRiver) {
            // River ends towards begin corner
            terrain.AddTriangle(begin, left, conInfo.leftRight);
            terrain.AddTriangleColor(beginCell.Color, leftCell.Color, conInfo.leftRightColor);
            terrain.AddTriangle(begin, conInfo.leftRight, right);
            terrain.AddTriangleColor(beginCell.Color, conInfo.leftRightColor, rightCell.Color);
        }
    }

    void TriangulateWaterfall(
        Vector3 v1, Vector3 v2,
        Vector3 v3, Vector3 v4,
        float y1, float y2, bool flowsClockwise
    ) {
        // v1 top left
        // v2 bottom left
        // v3 top right
        // v4 bottom right
        float totalHeight = v1.y - v2.y;
        float bottomRiverHeight = y2 - v2.y;
        float t = bottomRiverHeight / totalHeight;
        Vector3 v2_2 = Vector3.Lerp(v2, v1, t);
        Vector3 v4_2 = Vector3.Lerp(v4, v3, t);
        TriangulateRiverQuad(v1, v2_2, v3, v4_2, y1, y2, flowsClockwise);
    }

    void TriangulateRiverQuad(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4, float y1, float y2, bool flowsClockwise) {
        // v1 -> v3 -> v4 -> v2, as we follow each edge from the same start
        v1.y = v3.y = y1;
        v2.y = v4.y = y2;
        rivers.AddQuad(v1, v2, v3, v4);
        if (flowsClockwise) {
            rivers.AddQuadUV(0f, 1f, 0f, 1f);
        } else {
            rivers.AddQuadUV(1f, 0f, 1f, 0f);
        }
    }

    void TriangulateRiverQuad(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4, float y, bool flowsClockwise) {
        TriangulateRiverQuad(v1, v2, v3, v4, y, y, flowsClockwise);
    }

    void TriangulateRiverTriangle(Vector3 v1, Vector3 v2, Vector3 v3, float y, bool flowsClockwise) {
        v1.y = v2.y = v3.y = y;
        rivers.AddTriangle(v1, v2, v3);
        if (flowsClockwise) {
            rivers.AddTriangleUV(new Vector2(0f, 0f), new Vector2(0f, 1f), new Vector2(1f, 1f));
        } else {
            rivers.AddTriangleUV(new Vector2(1f, 1f), new Vector2(1f, 1f), new Vector2(0f, 0f));
        }
    }

    void TriangulateEdgeFan(Vector3 center, EdgeVertices edge, Color color) {
        // Draws the inner part of a cell
        terrain.AddTriangle(center, edge.v1, edge.v2);
        terrain.AddTriangleColor(color);
        terrain.AddTriangle(center, edge.v2, edge.v3);
        terrain.AddTriangleColor(color);
        terrain.AddTriangle(center, edge.v3, edge.v4);
        terrain.AddTriangleColor(color);
        terrain.AddTriangle(center, edge.v4, edge.v5);
        terrain.AddTriangleColor(color);
    }

    void TriangulateEdgeStrip(EdgeVertices e1, Color c1, EdgeVertices e2, Color c2, bool hasRoad = false) {
        // Draws the rectangular Edge between cells
        terrain.AddQuad(e1.v1, e1.v2, e2.v1, e2.v2);
        terrain.AddQuadColor(c1, c2);
        terrain.AddQuad(e1.v2, e1.v3, e2.v2, e2.v3);
        terrain.AddQuadColor(c1, c2);
        terrain.AddQuad(e1.v3, e1.v4, e2.v3, e2.v4);
        terrain.AddQuadColor(c1, c2);
        terrain.AddQuad(e1.v4, e1.v5, e2.v4, e2.v5);
        terrain.AddQuadColor(c1, c2);

        if (hasRoad) {
            TriangulateRoadSegment(e1.v2, e1.v3, e1.v4, e2.v2, e2.v3, e2.v4);
        }
    }

    void TriangulateOptionalWaterfall(
        Vector3 begin, HexCell beginCell,
    Vector3 left, HexCell leftCell,
    Vector3 right, HexCell rightCell,
    HexConnectionWithRiverInformation conInfo, HexDirection direction

    ) {
        if (conInfo.beginLeftSideHasRiver) {
            float waterSurfaceWaterfallEnd = conInfo.riverSurfaceLeftRight;
            if (rightCell.IsUnderwater) {
                waterSurfaceWaterfallEnd = rightCell.WaterSurfaceY;

            }
            float riverHeightDiff = conInfo.riverSurfaceBeginLeft - waterSurfaceWaterfallEnd;
            bool originatingRiverFlowDirection = beginCell.GetRiverFlowDirection(direction);
            // Only draw if the water falls down
            if (riverHeightDiff > 0 && originatingRiverFlowDirection) {
                TriangulateWaterfall(
                    begin, conInfo.beginRight, left, conInfo.leftRight,
                    conInfo.riverSurfaceBeginLeft, waterSurfaceWaterfallEnd,
                    true);
            }
        }
        if (conInfo.leftRightSideHasRiver) {
            float waterSurfaceWaterfallEnd = conInfo.riverSurfaceBeginRight;
            if (beginCell.IsUnderwater) {
                waterSurfaceWaterfallEnd = beginCell.WaterSurfaceY;
            }
            float riverHeightDiff = conInfo.riverSurfaceLeftRight - waterSurfaceWaterfallEnd;
            bool originatingRiverFlowDirection = rightCell.GetRiverFlowDirection(direction.Opposite().Previous());
            // Only draw if the water falls down
            if (riverHeightDiff > 0 && !originatingRiverFlowDirection) {
                TriangulateWaterfall(
                    left, conInfo.beginLeft, right, conInfo.beginRight,
                    conInfo.riverSurfaceLeftRight, waterSurfaceWaterfallEnd,
                    true);
            }
        }


        if (conInfo.beginRightSideHasRiver) {
            float waterSurfaceWaterfallEnd = conInfo.riverSurfaceBeginLeft;
            if (leftCell.IsUnderwater) {
                waterSurfaceWaterfallEnd = leftCell.WaterSurfaceY;
            }
            float riverHeightDiff = conInfo.riverSurfaceBeginRight - waterSurfaceWaterfallEnd;
            bool originatingRiverFlowDirection = beginCell.GetRiverFlowDirection(direction.Next());
            // Only draw if the water falls down
            if (riverHeightDiff > 0 && !originatingRiverFlowDirection) {
                TriangulateWaterfall(
                    right, conInfo.leftRight, begin, conInfo.beginLeft,
                    conInfo.riverSurfaceBeginRight, waterSurfaceWaterfallEnd,
                    true);
            }
        }
    }
}