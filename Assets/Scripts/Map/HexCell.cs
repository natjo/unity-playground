using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexCell : MonoBehaviour {
    [SerializeField] HexCell[] neighbors;
    [SerializeField] bool[] roads;
    // true means river flows clockwise
    [SerializeField] bool[] rivers;
    [SerializeField] bool[] riversFlowDirection;
    public HexGridChunk chunk;
    public HexCoordinates coordinates;
    public RectTransform uiRect;
    public Color Color {
        get { return color; }
        set {
            if (color == value) {
                return;
            }
            color = value;
            Refresh();
        }
    }
    public int Elevation {
        get { return elevation; }
        set {
            if (elevation == value) {
                return;
            }
            elevation = value;
            Vector3 position = transform.localPosition;
            position.y = value * HexMetrics.elevationStep;
            transform.localPosition = position;

            Vector3 uiPosition = uiRect.localPosition;
            uiPosition.z = elevation * -HexMetrics.elevationStep;
            uiRect.localPosition = uiPosition;

            for (int i = 0; i < roads.Length; i++) {
                if (roads[i] && GetElevationDifference((HexDirection)i) > 1) {
                    SetRoad(i, false);
                }
            }
            Refresh();
        }
    }
    public float RiverStreamBedYShared(HexDirection direction) {
        return Mathf.Min(RiverStreamBedY, neighbors[(int)direction].RiverStreamBedY);
    }
    public float RiverStreamBedY {
        get {
            return
                (elevation + HexMetrics.streamBedElevationOffset) *
                HexMetrics.elevationStep;
        }
    }
    public float RiverSurfaceYShared(HexDirection direction) {
        return Mathf.Min(RiverSurfaceY, neighbors[(int)direction].RiverSurfaceY);
    }
    public float RiverSurfaceY {
        get {
            return (elevation + HexMetrics.waterElevationOffset) * HexMetrics.elevationStep;
        }
    }
    public float WaterSurfaceY {
        get {
            return (waterLevel + HexMetrics.waterElevationOffset) * HexMetrics.elevationStep;
        }
    }
    public bool HasRoads {
        get {
            for (int i = 0; i < roads.Length; i++) {
                if (roads[i]) {
                    return true;
                }
            }
            return false;
        }
    }
    public int WaterLevel {
        get {
            return waterLevel;
        }
        set {
            if (waterLevel == value) {
                return;
            }
            waterLevel = value;
            Refresh();
        }
    }
    int waterLevel;

    int elevation = int.MinValue;
    Color color;

    public bool IsUnderwater {
        get {
            return waterLevel > elevation;
        }
    }

    public HexCell GetNeighbor(HexDirection direction) {
        return neighbors[(int)direction];
    }

    public void SetNeighbor(HexDirection direction, HexCell cell) {
        // Always set the current cell and its neighbor's neighbor at the same time
        neighbors[(int)direction] = cell;
        cell.neighbors[(int)direction.Opposite()] = this;
    }

    public Vector3 Position {
        get {
            return transform.localPosition;
        }
    }

    public void AddRiver(HexDirection direction, bool flowsClockwise) {
        SetRiver((int)direction, flowsClockwise, true);
    }

    public void RemoveRiver(HexDirection direction) {
        if (rivers[(int)direction]) {
            SetRiver((int)direction, true, false);
        }
    }

    public void SetRiver(int index, bool flowsClockwise, bool state) {
        rivers[index] = state;
        riversFlowDirection[index] = flowsClockwise;
        RefreshSelfOnly();
        neighbors[index].rivers[(int)((HexDirection)index).Opposite()] = state;
        neighbors[index].riversFlowDirection[(int)((HexDirection)index).Opposite()] = !flowsClockwise;
        neighbors[index].RefreshSelfOnly();
    }

    public bool HasRiver(HexDirection direction) {
        return rivers[(int)direction];
    }

    public bool GetRiverFlowDirection(HexDirection direction) {
        return riversFlowDirection[(int)direction];
    }

    public bool HasRoadThroughEdge(HexDirection direction) {
        return roads[(int)direction];
    }

    public void RemoveRoads() {
        for (int i = 0; i < roads.Length; ++i) {
            if (roads[i]) {
                SetRoad(i, false);
            }
        }
    }

    public void AddRoad(HexDirection direction) {
        if (!roads[(int)direction] && GetElevationDifference(direction) <= 1) {
            SetRoad((int)direction, true);
        }
    }

    void SetRoad(int index, bool state) {
        roads[index] = state;
        neighbors[index].roads[(int)((HexDirection)index).Opposite()] = state;
        RefreshSelfOnly();
        neighbors[index].RefreshSelfOnly();
    }

    public int GetElevationDifference(HexDirection direction) {
        int difference = elevation - GetNeighbor(direction).elevation;
        return difference >= 0 ? difference : -difference;
    }

    void Refresh() {
        if (chunk) {
            chunk.Refresh();
            for (int i = 0; i < neighbors.Length; i++) {
                HexCell neighbor = neighbors[i];
                if (neighbor != null && neighbor.chunk != chunk) {
                    neighbor.chunk.Refresh();
                }
            }
        }
    }

    void RefreshSelfOnly() {
        chunk.Refresh();
    }
}
