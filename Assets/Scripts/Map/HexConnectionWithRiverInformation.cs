using UnityEngine;

public class HexConnectionWithRiverInformation {
    public HexCell beginCell, leftCell, rightCell;

    public Vector3 beginLeft, beginRight, leftRight;
    public Vector3 beginLeftOriginPerturbed, beginRightOriginPerturbed, leftRightOriginPerturbed;
    public Color beginLeftColor, beginRightColor, leftRightColor;

    public bool beginLeftSideHasRiver, beginRightSideHasRiver, leftRightSideHasRiver;
    public bool riverEnds;
    public bool withRiver;

    public float RiverSurfaceY;
    public float riverSurfaceBeginLeft, riverSurfaceLeftRight, riverSurfaceBeginRight;

    public HexConnectionWithRiverInformation(
        Vector3 begin, HexCell beginCell,
        Vector3 left, HexCell leftCell,
        Vector3 right, HexCell rightCell,
        HexDirection direction
    ) {
        this.beginCell = beginCell;
        this.leftCell = leftCell;
        this.rightCell = rightCell;
        beginLeft = Vector3.Lerp(begin, left, 0.5f);
        beginRight = Vector3.Lerp(begin, right, 0.5f);
        leftRight = Vector3.Lerp(left, right, 0.5f);
        beginLeftOriginPerturbed = Vector3.Lerp(HexMetrics.Perturb(begin), HexMetrics.Perturb(left), 0.5f);
        beginRightOriginPerturbed = Vector3.Lerp(HexMetrics.Perturb(begin), HexMetrics.Perturb(right), 0.5f);
        leftRightOriginPerturbed = Vector3.Lerp(HexMetrics.Perturb(left), HexMetrics.Perturb(right), 0.5f);
        beginLeftColor = Color.Lerp(beginCell.Color, leftCell.Color, 0.5f);
        beginRightColor = Color.Lerp(beginCell.Color, rightCell.Color, 0.5f);
        leftRightColor = Color.Lerp(leftCell.Color, rightCell.Color, 0.5f);

        beginLeftSideHasRiver = beginCell.HasRiver(direction);
        beginRightSideHasRiver = beginCell.HasRiver(direction.Next());
        leftRightSideHasRiver = leftCell.HasRiver(direction.Opposite().Previous());
        // bool beginLeftSideHasWater = beginLeftSideHasRiver || beginCell.IsUnderwater || leftCell.IsUnderwater;
        // bool leftRightSideHasWater = leftRightSideHasRiver || rightCell.IsUnderwater || leftCell.IsUnderwater;
        // bool beginRightSideHasWater = beginRightSideHasRiver || beginCell.IsUnderwater || rightCell.IsUnderwater;
        riverEnds =
            (beginLeftSideHasRiver && !beginRightSideHasRiver && !leftRightSideHasRiver) ||
            (!beginLeftSideHasRiver && beginRightSideHasRiver && !leftRightSideHasRiver) ||
            (!beginLeftSideHasRiver && !beginRightSideHasRiver && leftRightSideHasRiver);
        withRiver = beginLeftSideHasRiver || beginRightSideHasRiver || leftRightSideHasRiver;

        riverSurfaceBeginLeft = beginCell.RiverSurfaceYShared(direction);
        riverSurfaceLeftRight = rightCell.RiverSurfaceYShared(direction.Previous());
        riverSurfaceBeginRight = beginCell.RiverSurfaceYShared(direction.Next());

        if (beginLeftSideHasRiver) {
            beginLeft.y = beginLeftOriginPerturbed.y = Mathf.Min(beginCell.RiverStreamBedY, leftCell.RiverStreamBedY);
        }
        if (beginRightSideHasRiver) {
            beginRight.y = beginRightOriginPerturbed.y = Mathf.Min(beginCell.RiverStreamBedY, rightCell.RiverStreamBedY);
        }
        if (leftRightSideHasRiver) {
            leftRight.y = leftRightOriginPerturbed.y = Mathf.Min(rightCell.RiverStreamBedY, leftCell.RiverStreamBedY);
        }

        RiverSurfaceY = Mathf.Min(beginCell.RiverSurfaceY, leftCell.RiverSurfaceY, rightCell.RiverSurfaceY);

    }
}