using UnityEngine;

public static class HexMetrics {
    // Map settings
    public const int chunkSizeX = 5, chunkSizeZ = 5;
    // Per hexagon settings
	public const float outerToInner = 0.866025404f;
	public const float innerToOuter = 1f / outerToInner;
    public const float outerRadius = 10f;
    public const float innerRadius = outerRadius * outerToInner;
    public const float solidFactor = 0.8f;
    public const float blendFactor = 1f - solidFactor;

    // Terrace related settings
    public const float elevationStep = 2f;
    public const int terracesPerSlope = 3;
    public const int terraceSteps = terracesPerSlope * 2 + 1;
    public const float horizontalTerraceStepSize = 1f / terraceSteps;
    public const float verticalTerraceStepSize = 1f / (terracesPerSlope + 1);

    // Water related settings
    public const float streamBedElevationOffset = -1.75f;
    public const float waterElevationOffset = -0.3f;
    public const float waterFactor = 0.6f;
    public const float waterBlendFactor = 1 - waterFactor;

    // Noise Settings
    public static Texture2D noiseSource;
    public const float noiseAmplitude = 4f;
    // We use a different noise amplitude for elevation, to keep them flatter
    public const float elevationNoiseAplitude = 1.5f;
    public const float noiseScale = 0.003f;

    static Vector3[] corners = {
        new Vector3(0f, 0f, outerRadius),
        new Vector3(innerRadius, 0f, 0.5f * outerRadius),
        new Vector3(innerRadius, 0f, -0.5f * outerRadius),
        new Vector3(0f, 0f, -outerRadius),
        new Vector3(-innerRadius, 0f, -0.5f * outerRadius),
        new Vector3(-innerRadius, 0f, 0.5f * outerRadius),
        new Vector3(0f, 0f, outerRadius)
    };

    public static Vector3 GetFirstCorner(HexDirection direction) {
        return corners[(int)direction];
    }

    public static Vector3 GetSecondCorner(HexDirection direction) {
        return corners[(int)direction + 1];
    }
    public static Vector3 GetFirstSolidCorner(HexDirection direction) {
        return GetFirstCorner(direction) * solidFactor;
    }

    public static Vector3 GetSecondSolidCorner(HexDirection direction) {
        return GetSecondCorner(direction) * solidFactor;
    }

    public static Vector3 GetSolidEdgeMiddle(HexDirection direction) {
        return
            (corners[(int)direction] + corners[(int)direction + 1]) *
            (0.5f * solidFactor);
    }

    public static Vector3 GetFirstWaterCorner(HexDirection direction){
        return corners[(int) direction] * waterFactor;
    }

    public static Vector3 GetSecondWaterCorner(HexDirection direction){
        return corners[(int) direction + 1] * waterFactor;
    }

    public static Vector3 GetBridge(HexDirection direction) {
        // Returns the inner corner projected against the outer hexagon wall
        return (corners[(int)direction] + corners[(int)direction + 1]) * blendFactor;
    }

    public static Vector3 GetWaterBridge(HexDirection direction) {
        // Returns the inner corner projected against the outer hexagon wall
        return (corners[(int)direction] + corners[(int)direction + 1]) * waterBlendFactor;
    }

    public static Vector3 GetPointInBetween(Vector3 v1, Vector3 v2, float y){
        Vector3 v2tov1 = v2-v1;
        return v2 - v2tov1 * (y - v2.y) / Vector3.Magnitude(v2tov1);
    }

    public static Vector3 Perturb(Vector3 position) {
        Vector4 sample = SampleNoise(position);
        position.x += (sample.x * 2f - 1f) * noiseAmplitude;
        position.y += (sample.y * 2f - 1f) * elevationNoiseAplitude;
        position.z += (sample.z * 2f - 1f) * noiseAmplitude;
        return position;
    }

    public static Vector4 SampleNoise(Vector3 position) {
        return noiseSource.GetPixelBilinear(
            position.x * noiseScale,
            position.z * noiseScale
            );
    }

}
