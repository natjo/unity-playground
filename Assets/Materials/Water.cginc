float Foam (float shore, float2 worldXZ, sampler2D noiseTex){
	shore = sqrt(shore) * 0.9;

    float2 noiseUV = worldXZ + _Time.y * 0.25;
    float4 noise = tex2D(noiseTex, noiseUV * 0.1);

    float distortion1 = noise.x * (1 - shore);
    float foam1 = sin((shore + distortion1) * 15 - _Time.y);
    foam1 *= foam1;

    float distortion2 = noise.x * (1 - shore);
    float foam2 = sin((shore + distortion1) * 15 + _Time.y + 2);
    foam2 *= foam2 * 0.5;

    return max(foam1, foam2) * shore * 0.45;
}

float Waves (float2 worldXZ, sampler2D noiseTex){
    float2 uv1 = worldXZ;
    uv1.y += _Time.y * 0.5;
    float4 noise1 = tex2D(noiseTex, uv1 * 0.05);

    float2 uv2 = worldXZ;
    uv1.x += _Time.y * 0.5;
    float4 noise2 = tex2D(noiseTex, uv1 * 0.05);

    // Line shaped wave
    float blendWave = sin((worldXZ.x + worldXZ.y) * 0.7 + (noise1.y + noise2.x) + _Time.y);
    blendWave *= blendWave; // To ensure the sinus is always positive
    blendWave = saturate(blendWave + 0.5);

    float waves = noise1.z + noise2.x;
    return smoothstep(0.75, 2, waves) * blendWave;
}

float River (float2 riverUV, sampler2D noiseTex){
    float2 uv = riverUV;
    // Stretch the noise texture to make it more "wavy", less "pointy"
    uv.y = uv.y * 0.1 + _Time.y * 0.005;
    uv.x -= _Time.y * 0.1;
    float4 noise = tex2D(noiseTex, uv);

    // Add a second layer of noise
    float2 uv2 = riverUV;
    // Stretch the noise texture to make it more "wavy", less "pointy"
    uv2.y = uv2.y * 0.06 - _Time.y * 0.0052;
    uv2.x -= _Time.y * 0.08;
    float4 noise2 = tex2D(noiseTex, uv2);

    // return noise.x * noise2.w;
    return noise.r * noise2.g;
}